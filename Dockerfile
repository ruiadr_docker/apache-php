FROM php:8.1.20-apache-bullseye

RUN set -x \
    && export DEBIAN_FRONTEND=noninteractive \
    && echo 'deb http://http.debian.net/debian bullseye-backports contrib non-free main' >> /etc/apt/sources.list \
    && apt update && apt upgrade -y -t bullseye-backports \
    && apt install --no-install-recommends -t bullseye-backports -y \
        runit tini sudo git zip unzip gnupg nmap locales default-mysql-client libcurl3-gnutls \
        default-libmysqlclient-dev libgmp-dev libsodium-dev libzip-dev libcurl4-gnutls-dev \
        zlib1g-dev libxml2-dev libfreetype-dev libjpeg62-turbo-dev libpng-dev libjpeg-dev libwebp-dev \
        unattended-upgrades \
    # locale
    && sed -i 's/^# *\(fr_FR.UTF-8 UTF-8\)/\1/' /etc/locale.gen && locale-gen \
    # php
    && pecl install apcu \
    && echo 'extension=apcu.so' > /usr/local/etc/php/conf.d/apcu.ini \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp --enable-gd \
    && docker-php-ext-install opcache pdo_mysql mysqli bcmath gmp zip exif gd \
    # composer
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    # apache
    && a2enmod expires headers deflate rewrite remoteip \
    # unattended upgrades, exec from docker host with docker command
    # docker exec -u root CONTAINER_NAME sh -c "/usr/bin/unattended-upgrades"
    && echo 'APT::Periodic::Download-Upgradeable-Packages "1";' >> /etc/apt/apt.conf.d/20auto-upgrades \
    && echo 'APT::Periodic::AutocleanInterval "7";' >> /etc/apt/apt.conf.d/20auto-upgrades \
    && sed -i 's/^  \(      "origin=Debian,codename=${distro_codename},label=Debian";\)/\/\/\1/' /etc/apt/apt.conf.d/50unattended-upgrades \
    # clean
    && apt-get autoremove -y && rm -rf /var/lib/apt/* /tmp/* /var/tmp/* \
    # service
    && mkdir -p /etc/service/apache \
    && ln -s /usr/local/bin/apache2-foreground /etc/service/apache/run

USER www-data

COPY run.sh /run.sh
COPY index.html /var/www/html

EXPOSE 80

ENTRYPOINT ["/usr/bin/tini"]

CMD ["/run.sh"]